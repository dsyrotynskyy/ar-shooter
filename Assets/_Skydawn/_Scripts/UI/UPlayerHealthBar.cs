﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UPlayerHealthBar : MonoBehaviour {

	public Image foreground;

	EntityHealth _hp;
	PlayerAvatar _player;
	void Start () {
		_player = PlayerAvatar.GetInstance;
		_player.AttackEnabledChangedEvent.AddListener(UpdateUI);
		_hp = _player.EntityHealth;
		_hp.OnHealthChangedEvent.AddListener (UpdateUI);

		UpdateUI ();
	}

	float _targetHP = 1f;

	void UpdateUI () {
		gameObject.SetActive(_player.InGame);
		_targetHP = _hp.HealthPercent;
	}

	void FixedUpdate () {
        foreground.fillAmount = Mathf.Lerp (foreground.fillAmount, _targetHP, Time.fixedDeltaTime * 10f);
	}
}
