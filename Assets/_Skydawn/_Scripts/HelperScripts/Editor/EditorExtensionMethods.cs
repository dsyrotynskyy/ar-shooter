﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

public static class EditorExtensionMethods
{
    public static bool DisplayButton(this Editor editor, string text, bool buttonEnabled)
    {
        GUI.enabled = buttonEnabled;
        bool res = GUILayout.Button(text);
        GUI.enabled = true;
        return res;
    }

    public static UnityEngine.Object GetPrefab(this Editor editor, MonoBehaviour mono)
    {
        PrefabType t = PrefabUtility.GetPrefabType(mono.gameObject);
        if (t == PrefabType.Prefab)
        {
            return mono.gameObject;
        }
        else
        {
            return PrefabUtility.GetCorrespondingObjectFromSource(mono.gameObject);
        }
    }

    public static void SavePrefab(this Editor editor)
    {
        SavePrefab(editor, (MonoBehaviour)editor.target);
    }

    public static void SavePrefab(this Editor editor, MonoBehaviour mono)
    {
        SavePrefab(mono);
    }

    public static void SavePrefab (MonoBehaviour mono)
    {
        var instance = PrefabUtility.FindRootGameObjectWithSameParentPrefab(mono.gameObject);
        if (instance == null)
        {
            return;
        }
        bool wasActive = instance.activeSelf;
        instance.SetActive(true);
        PrefabUtility.ReplacePrefab(instance, PrefabUtility.GetCorrespondingObjectFromSource(instance), ReplacePrefabOptions.ConnectToPrefab);
        instance.SetActive(wasActive);
    }

    public static GameObject GetPrefabGameObject(this Editor editor)
    {
        return (GameObject)GetPrefab(editor, (MonoBehaviour)editor.target);
    }
}