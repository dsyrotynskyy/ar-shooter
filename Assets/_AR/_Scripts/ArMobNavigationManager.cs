﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArMobNavigationManager : MonoBehaviour
{
    public Vector3 destination;
    public bool isStopped;
    public float stoppingDistance = 0.5f;
    public float velocity;
    float _velocity;
    public float radius { get { return _collider.radius; } }
    public float speed = 2f;
    float initialY;

    Rigidbody _rigidbody;
    CapsuleCollider _collider;

    void Start()
    {
        destination = transform.position;
        _prevPos = transform.position;
        _rigidbody = GetComponent<Rigidbody>();
        initialY = transform.position.y;

        _collider = GetComponent<CapsuleCollider>();
        _collider.radius = radius;

        PerformRotation();
    }

    Vector3 _prevPos;
 
    void Update()
    {
        PerformRotation();
    }

    private void FixedUpdate()
    {
        PerformMovement();
        CalculateVelocity();
    }

    void CalculateVelocity ()
    {
        var offset = (transform.position - _prevPos);
        _velocity = (offset.magnitude) / Time.fixedDeltaTime;
        _prevPos = transform.position;
        velocity = _velocity;
    }

    void PerformRotation()
    {
        var target = destination;
        target.y = destination.y - 0.5f;

        Vector3 vector = target - transform.position;
        float distance = vector.magnitude;
        if (distance > radius + stoppingDistance)
        {
            transform.LookAt(target);
        }

        var e = transform.eulerAngles;
        e.x = 0f;
        e.z = 0f;
        transform.eulerAngles = e;
    }

    void PerformMovement ()
    {
        if (!PlayerAvatar.GetInstance.InGame)
        {
            _rigidbody.velocity = Vector3.zero;
            return;
        }

        var target = destination;
        target.y = initialY;
        target.y = destination.y - 0.5f;
        target.y = transform.position.y;
        var vector = transform.position - destination;
        float distance = vector.magnitude;
        if (distance > radius + stoppingDistance)
        {
            var newPos = Vector3.MoveTowards(transform.position, target, speed * Time.fixedDeltaTime);
            _rigidbody.MovePosition(newPos);
        } else
        {
            _rigidbody.velocity = Vector3.zero;
        }
    }
}
