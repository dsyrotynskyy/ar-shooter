﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UPlayerRage : MonoBehaviour {

    public Color rageColor;
    public Color normalColor;
    public Image foreground;

    PlayerAvatar _player;
    void Start()
    {
        _player = PlayerAvatar.GetInstance;

        _player.OnRageValueChangedEvent.AddListener(UpdateUI);

        UpdateUI();
    }

    float _targetRage = 1f;

    void UpdateUI()
    {
        _targetRage = _player.RagePower;
        foreground.fillAmount = _targetRage;
        if (_player.IsRage) {
            foreground.color = rageColor;
        } else {
            foreground.color = normalColor;
        }
        CheckActive();
    }

    void CheckActive () {
        if (_targetRage <= 0f && _player.AttackTarget == null)
        {
            gameObject.SetActive(false);
        } else {
            gameObject.SetActive(true);
        }
    }
}
