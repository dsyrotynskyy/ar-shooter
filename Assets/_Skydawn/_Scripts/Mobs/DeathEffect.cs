﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DeathEffect : MonoBehaviour
{
    public float deactivateGoDelay = 0f;
    public GameObject deathParticles;
    internal EnemyPortal portal;

    // Start is called before the first frame update
    void Start()
    {
        EntityHealth EntityHealth = GetComponent<EntityHealth>();
        EntityHealth.OnDiedEvent.AddListener(OnDied);
    }

    private void OnDied()
    {
        // Ease ease = Ease.InCirc;
        // float time = 1f;
        // transform.DOMove(portal.transform.position, time).SetEase(ease);
        // var t = transform.DOScale(Vector3.zero, time * 2f).SetEase(ease);
        // t.onComplete += ()=> {
        //     gameObject.SetActive(false);
        // };

        var effect = GameObject.Instantiate(deathParticles);
        effect.transform.position = transform.position;
        if (deactivateGoDelay >= 0f) {
            this.Invoke(deactivateGoDelay, ()=> {gameObject.SetActive(false);});
        }
    }
}
