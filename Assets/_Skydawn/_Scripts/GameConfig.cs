﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConfig : UnitySingleton<GameConfig> {

    [Header("Settings")]
    public float EnemyAttackPower = 3;
    public float EnemyRadius = 1;
    public float PlayerAttackRadius = 0.33f;
    public int PlayerHealth = 100;
    public int PlayerAttackType; // 1 = autoattack, 2 = touch attack, 3 = swipe attack
    public float restoreManaPercent = 0.1f;

    void Start () {
        Application.targetFrameRate = 60;
	}

    protected override bool OrderDontDestroyOnLoad
    {
        get
        {
            return true;
        }
    }

    public bool IsAutoAttack
    {
        get
        {
            return PlayerAttackType <= 1;
        }
    }

    public bool IsSwipeAttack
    {
        get
        {
            return PlayerAttackType >= 3;
        }
    }

    public bool IsTouchAttack
    {
        get
        {
            return PlayerAttackType == 2;
        }
    }
}
