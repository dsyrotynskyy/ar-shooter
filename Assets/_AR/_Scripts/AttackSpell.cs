﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackSpell : MonoBehaviour
{
    public float manaUsage = 10f;
    public float attackPower = 5f;

    public GameObject spellCore;

    void Start()
    {
        EffectSettings effectSettings = GetComponent<EffectSettings>();
        effectSettings.CollisionEnter += EffectSettings_OnCollisionHandler;
    }

    public AudioClip attackAudio;

    private void EffectSettings_OnCollisionHandler(object sender, CollisionInfo e)
    {
        if (!e.Hit.collider)
        {
            return;
        }

        if (spellCore) {
            spellCore.SetActive (false);
        }

        onCollisionEvent.Invoke (this, e.Hit.collider);

        AudioPlayer.GetOrCreateInstance.Play(attackAudio);
    }

    public TemplateEvent<AttackSpell, Collider> onCollisionEvent = new TemplateEvent<AttackSpell, Collider> ();
}
