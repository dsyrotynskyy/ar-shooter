﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIManager : UnitySingleton<AIManager>
{
	public int CountDeadMobs () {
		int accum = 0;
		for (int i = 0; i < this._allMobs.Count; i++) {
			var m = _allMobs[i];
			if (!m.EntityHealth.IsAlive) {
				accum++;
			}
		}
		return accum;
	}

	PlayerAvatar _playerAvatar;
//	List<IEntity> _allEntities = new List<IEntity> ();
	List<IAIMob> _allMobs = new List<IAIMob> ();

	public float AIActivationDistance = 90f;
	public float AIAggroLeaveDistance = 20f;
	public float AIAggroDistance = 10f;

	void Awake ()
	{
		_playerAvatar = PlayerAvatar.GetInstance;
		InvokeRepeating ("CalculateAI", 0.1f, 1f);
	}

	public void RegisterMob (IAIMob mob)
	{
		_allMobs.Add (mob);
//		_allEntities.Add (mob);
	}
		
	public void CalculateAI ()
	{
		CalculateActiveEntities ();
		CalculateDistances ();
		CalculateAttackTargets ();
	}

	void CalculateActiveEntities ()
	{
		float sqrActivation = AIActivationDistance * AIActivationDistance;
		float sqrAggroLeave = AIAggroLeaveDistance * AIAggroLeaveDistance;
		float sqrAggroDistance = AIAggroDistance * AIAggroDistance;
		
		for (int i = 0; i < _allMobs.Count; i++) {
			var mob = _allMobs [i];
		
			float sqrDistanceToPlayer = (_playerAvatar.transform.position - mob.transform.position).sqrMagnitude;
		
			if (sqrDistanceToPlayer > sqrActivation) {
//				mob.SetEntityActive (false);
			} else {
				mob.SetEntityActive (true);
				mob.PrepareCalculationsAI (sqrAggroDistance, sqrAggroLeave);

				mob.SetSqrDistance (_playerAvatar, sqrDistanceToPlayer);
			}
		}
	}

	void CalculateDistances () {
		for (int i = 0; i < _allMobs.Count; i++) {
			var mob1 = _allMobs [i];
			if (!mob1.IsActive && !mob1.EntityHealth.IsAlive) {
				continue;
			}
			for (int j = i + 1; j < _allMobs.Count; j++) {
				var mob2 = _allMobs [j];
				if (!mob2.IsActive) {
					continue;
				}

				if (!mob1.HasSqrDistance (mob2)) {
					float sqrDistance = (mob1.transform.position - mob2.transform.position).sqrMagnitude;
					mob1.SetSqrDistance (mob2, sqrDistance);
					mob2.SetSqrDistance (mob1, sqrDistance);
				}
			}
		}
	}

	void CalculateAttackTargets () {
		for (int i = 0; i < _allMobs.Count; i++) {
			var mob = _allMobs [i];
			if (!mob.IsActive) {
				continue;
			}
			mob.CalculateTargetByDistance ();
		}

		for (int i = 0; i < _allMobs.Count; i++) {
			var mob = _allMobs [i];
			if (!mob.IsActive) {
				continue;
			}
			mob.CalculateTargetsByFriends ();
			mob.EndAiCalculations ();
		}
	}

	//	void CalculateAIOld () {
	//		float sqrActivation = AIActivationDistance * AIActivationDistance;
	//		float sqrAggroLeave = AIAggroLeaveDistance * AIAggroLeaveDistance;
	//		float sqrAggroDistance = AIAggroDistance * AIAggroDistance;
	//
	//		for (int i = 0; i < _allEntities.Count; i++) {
	//			var mob = _allEntities [i];
	//
	//			float sqrDistanceToPlayer = (_playerAvatar.transform.position - mob.transform.position).sqrMagnitude;
	//
	//			if (sqrDistanceToPlayer > sqrActivation) {
	//				mob.SetMobActive (false);
	//			} else {
	//				mob.SetMobActive (true);
	//				mob.UpdateChaseTarget (_playerAvatar.EntityHealth, sqrAggroLeave, sqrAggroDistance, sqrDistanceToPlayer);
	//			}
	//		}
	//	}
}

public interface IEntity
{
    Transform spellAttackAnchor{ get; }
	Transform transform { get; }
	int team {get;}
	EntityHealth EntityHealth { get;}
	IEntity AttackTarget { get;}
	float GetSqrDistance (IEntity entity);
}

public interface IAIMob : IEntity
{
	void SetEntityActive (bool active);
	bool IsActive { get; }
	void PrepareCalculationsAI (float sqrAggroDistancePeace, float sqrAggroDistanceChase);
	bool HasSqrDistance (IEntity entity);
	void SetSqrDistance (IEntity entity, float sqrDistance);
	void CalculateTargetByDistance ();
	void CalculateTargetsByFriends ();
	void EndAiCalculations();
}
