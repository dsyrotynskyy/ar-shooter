﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class MobAnimatorController : MonoBehaviour {

	internal Transform target;

	void Awake () {
		_anim = GetComponent<Animator>();
	}

	Animator _anim;

	void Start()
	{
		UpdateState();
	}

	public bool IsPlayingAttackAnimation
	{
		get
		{
			return _attack;
			// if (_anim.GetCurrentAnimatorStateInfo(0).IsName("_attack"))
			// {
			// 	return true;
			// }
			// else
			// {
			// 	return false;
			// }
		}
	}

	float _distance;

	void Update() {
        UpdateRotation();
	}

	public void OrderDead () {
  //      _anim.SetInteger("attack", 0);
		_anim.SetTrigger ("dead");
		enabled = false;
	}

    public void OrderAttack (bool attack) {
        _attack = attack;
    }

    public void OrderMove (bool move) {
        _move = move;
    }

    public bool IsMovementOrdered {
        get {
            return _move;
        }
    }

    bool _move;
    bool _attack = false;

	void UpdateRotation () {
		if (_attack) {
			if (target) {
				Vector3 dir = target.position - transform.position;
				dir.y = 0f;
				Quaternion trot = Quaternion.LookRotation (dir);
				transform.rotation = Quaternion.Lerp (transform.rotation, trot, Time.deltaTime * 5f);
			}
		} 
	}

    public void UpdateState () {
        UpdateState(_move, _attack);
    }

    public void UpdateState(bool shouldMove, bool shouldAttack)
	{
		if (shouldAttack)
		{
			shouldMove = false;
		}

		_anim.SetBool("move", shouldMove);
		_anim.SetInteger("attack", shouldAttack ? 1 : 0);
	}

	public void OnAnimatorAttack () {
//		Debug.Log ("On animator attack");

		if (_attack) {
			OnAttackEvent.Invoke ();
		}
	}

	internal UnityEvent OnAttackEvent = new UnityEvent ();
}
