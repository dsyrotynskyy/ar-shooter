﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.XR;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARPortalSpawner : MonoBehaviour
{
    public GameObject objectToPlace;
    public GameObject placementIndicator;

    private ARSessionOrigin arOrigin;
    private ARRaycastManager aRRaycastManager;
    private Pose placementPose;
    private bool placementPoseIsValid = false;

    public bool PlacementPoseIsValid {
        get {
            return placementPoseIsValid;
        }
    }

    public Button tapButton;

    void Start()
    {
        arOrigin = FindObjectOfType<ARSessionOrigin>();
                aRRaycastManager = arOrigin.GetComponent<ARRaycastManager>();
        //       tapButton.onClick.AddListener(PlaceObject);
        Init();
    }

    bool PortalPlaced = false;

    void LateUpdate()
    {
        if (PortalPlaced)
        {
            return;
        }

        if (!Application.isEditor)
        {
            UpdatePlacementPose();
        } else {
            this.placementPose.position = transform.position;
            this.placementPose.rotation = transform.rotation;
            placementPoseIsValid = true;
        }

        UpdatePlacementIndicator();

        //if (placementPoseIsValid)
        //{
        //    if (Input.GetMouseButtonDown(0))
        //    {
        //        this.Invoke("PlaceObject", 0.1f);
        //    }
        //}
    }

    private void UpdatePlacementPose()
    {
        var screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        aRRaycastManager.Raycast(screenCenter, hits, TrackableType.Planes);

        placementPoseIsValid = hits.Count > 0;
        Debug.Log("Pose is valid = " + placementPoseIsValid);
        if (placementPoseIsValid)
        {
            placementPose = hits[0].pose;

            var cameraForward = Camera.current.transform.forward;
            var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
            placementPose.rotation = Quaternion.LookRotation(cameraBearing);
        }
    }

    private void UpdatePlacementIndicator()
    {
        if (placementPoseIsValid)
        {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
        else
        {
            placementIndicator.SetActive(false);
        }
    }

    public EnemyPortal PlaceObject()
    {
        if (!placementPoseIsValid)
        {
            return null;
        }
        if (!this.gameObject.activeSelf)
        {
            return null;
        }
        Debug.Log("AR Blade: spawn portal");
        var res = Instantiate(objectToPlace, placementPose.position, placementPose.rotation);
        
        res.SetActive(true);

 //       res.transform.LookAt(PlayerAvatar.GetInstance.transform);
        var e = transform.eulerAngles;
        e.x = 0f;
        e.z = 0f;
        transform.eulerAngles = e;

        PortalPlaced = true;
        OnPortalPlaced ();

        var portal = res.GetComponent<EnemyPortal> ();
        onPortalSpawnedEvent.Invoke(portal);
        return portal;
    }

    public TemplateEvent<EnemyPortal> onPortalSpawnedEvent = new TemplateEvent<EnemyPortal>();


    void Init ()
    {
        PlayerAvatar.GetInstance.InGame = false;
    }

    private void OnPortalPlaced()
    {

 //       placer.gameObject.SetActive(!PlayerAvatar.GetInstance.AttackEnabled);

        // placeEnemyButton.gameObject.SetActive(!PlayerAvatar.GetInstance.AttackEnabled);
        // attackButtonRoot.SetActive(PlayerAvatar.GetInstance.AttackEnabled);
    }
}
