﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobSoundManager : MonoBehaviour {

    public AudioClip[] onDamagedSounds;

    public void OnDamaged()
    {
        AudioClip s;
        s = onDamagedSounds.GetRandomElement<AudioClip>();

        var a = AudioPlayer.GetOrCreateInstance.Play(s);
        a.pitch = UnityEngine.Random.Range(0.9f, 1.1f);
    }
}
