﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class AvatarMagicSpawner : MonoBehaviour
{
    public AvatarInteractionPoint targetPoint;
    public float speed = 10f;
    public Transform spawnPosition;
    public AttackSpell projectilePrefab;
    Camera _mainCamera;

    void Start()
    {
        _mainCamera = Camera.main;
        PlayerAvatar = PlayerAvatar.GetInstance;
        EntityMana = PlayerAvatar.GetComponent<EntityMana>();
    }

    PlayerAvatar PlayerAvatar { get; set; }
    EntityMana EntityMana { get; set; }
    public void OnSpellCollided (AttackSpell spell, Collider collider) {
        var entity = collider.gameObject.GetComponent<EntityHealth>();
        if (entity) {
            entity.TakeDamage(spell.attackPower, null);
        }
    }

    public GameObject attackPoint;

    //void Update()
    //{
    //    if (Input.GetMouseButtonDown(0))
    //    {
    //        Spawn();
    //    }
    //}

    public void Spawn ()
    {
        if (!this.gameObject.activeInHierarchy) {
            return;
        }
        if (!EntityMana.UseMana (projectilePrefab.manaUsage))
        {
            return;
        }
        if (!PlayerAvatar.EntityHealth.IsAlive)
        {
            return;
        }
        var res = Instantiate(projectilePrefab, transform.position, transform.rotation);
        var spell = res.GetComponent<EffectSettings>();
        spell.Target = CreateAttackPoint ();
        res.onCollisionEvent.AddListener (OnSpellCollided);

        onSpellSpawnedEvent.Invoke();
    }

    GameObject CreateAttackPoint () {
        var a = GameObject.Instantiate(attackPoint);
        a.transform.position = attackPoint.transform.position;
        return a;
//        return attackPoint;
    }

    public UnityEvent onSpellSpawnedEvent = new UnityEvent ();

    Transform GetBestTarget ()
    {
        Transform bestTarget = null;
        float closestDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = transform.position;
        foreach (var e in this.targetPoint.mobs)
        {
            if (!e.EntityHealth.IsAlive)
            {
                continue;
            }
            Transform potentialTarget = e.spellAttackAnchor;
            Vector3 directionToTarget = potentialTarget.position - currentPosition;
            float dSqrToTarget = directionToTarget.sqrMagnitude;
            if (dSqrToTarget < closestDistanceSqr)
            {
                closestDistanceSqr = dSqrToTarget;
                bestTarget = potentialTarget;
            }
        }

        if (bestTarget == null)
        {
            return targetPoint.transform;
        }

        return bestTarget;
    }
}
