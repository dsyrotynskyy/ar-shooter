﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AvatarInteractionPoint : MonoBehaviour {

    public bool isConfiguredByPlayerMeleeAttackRadius;

    private void Start()
    {
        if (isConfiguredByPlayerMeleeAttackRadius)
        {
            var s = GetComponent<SphereCollider>();
            s.radius = GameConfig.GetInstance.PlayerAttackRadius;
        }
    }

    void OnTriggerEnter(Collider c) {
		if (c.CompareTag ("Mob")) {
			var mob = c.GetComponent<IEntity> ();
            mobs.Add(mob);

            mobEnteredEvent.Invoke (mob);
		}
	}

	void OnTriggerExit(Collider c)
	{
		if (c.CompareTag ("Mob")) {
			var mob = c.GetComponent<IEntity> ();
            mobs.Remove(mob);
			mobExitEvent.Invoke (mob);
		}
	}

    public List<IEntity> mobs = new List<IEntity>();

    public class MobEvent : UnityEvent <IEntity> {}

	public MobEvent mobEnteredEvent = new MobEvent ();
	public MobEvent mobExitEvent = new MobEvent ();
}
