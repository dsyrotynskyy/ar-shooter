﻿using System;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public class AIMob : MonoBehaviour, IAIMob
{
    public Transform SpellAttackAnchor;
    public Transform spellAttackAnchor { get { return SpellAttackAnchor; } }
    public int team = 0;
	EntityHealth _entityHealth;
	internal UnityEvent OnTargetCalculatedEvent = new UnityEvent();

	void Start () {
		_entityHealth = GetComponent<EntityHealth> ();
		AIManager.GetInstance.RegisterMob (this);
		AIManager.GetInstance.CalculateAI ();
	}

	#region IAIMob

	public bool IsChase {
		get {
			return !System.Object.ReferenceEquals (AttackTarget, null);
		}
	}

	public void SetEntityActive (bool active)
	{
        bool wasActive = gameObject.activeSelf;

        if (!wasActive && !EntityHealth.IsAlive)
        {
            return;
        }

		if (gameObject.activeSelf != active) {
			gameObject.SetActive (active);
			AttackTarget = null;
		}


	}

	public IEntity AttackTarget { get; private set;}
	Dictionary<IEntity, float> _sqrDistances = new Dictionary<IEntity, float>();
	List<IEntity> _friends = new List<IEntity> (); 
	float _candidateTargetDistance;
	IEntity _candidateTarget;
	float _sqrAggroDistancePeace;
	float _sqrAggroDistanceChase;

	public void PrepareCalculationsAI (float sqrAggroDistancePeace, float sqrAggroDistanceChase)
	{
		_sqrAggroDistancePeace = sqrAggroDistancePeace;
		_sqrAggroDistanceChase = sqrAggroDistanceChase;
		_friends.Clear ();
		_sqrDistances.Clear ();
		_candidateTargetDistance = int.MaxValue;
		_candidateTarget = null;
	}

	public bool HasSqrDistance (IEntity entity)
	{
		return _sqrDistances.ContainsKey (entity);
	}

	public float GetSqrDistance (IEntity entity) {
		return _sqrDistances [entity];
	}

	public void SetSqrDistance (IEntity entity, float sqrDistance)
	{
		_sqrDistances [entity] = sqrDistance;
		if (entity.team == this.team) {
            if (sqrDistance < _sqrAggroDistancePeace) {
				_friends.Add (entity);
			}
		} else {
			TrySetupCandidateTarget (entity, sqrDistance);
		}
	}

	void TrySetupCandidateTarget (IEntity entity, float sqrDistance) {
		if (_candidateTargetDistance > sqrDistance) {
			_candidateTargetDistance = sqrDistance;
			_candidateTarget = entity;
		}
	}

	bool CanChaseTarget (IEntity newTarget) {
		float comparisonDistance;
		if (IsChase) {
			comparisonDistance = _sqrAggroDistanceChase;
		} else {
			comparisonDistance = _sqrAggroDistancePeace;
		}
		if (_candidateTargetDistance <= comparisonDistance) {
			return true;
		} else {
			return false;
		}
	}

	public void CalculateTargetByDistance ()
	{
		TryChaseCandidateTarget ();
	}

	public void CalculateTargetsByFriends ()
	{
		if (!System.Object.ReferenceEquals (_candidateTarget, null)) {
			return;
		}

		for (int i = 0; i < _friends.Count; i++) {
			var f = _friends [i];
			if (f.AttackTarget != null) {
                AttackTarget = f.AttackTarget;
                Debug.Log("Chase friends target");
				//float sqrDistance = f.GetSqrDistance (f.AttackTarget);
				//TrySetupCandidateTarget (f.AttackTarget, sqrDistance);
			}
		}

//		TryChaseCandidateTarget ();
	}

	void TryChaseCandidateTarget () {
		if (CanChaseTarget (_candidateTarget)) {
			AttackTarget = _candidateTarget;
        } else {
            AttackTarget = null;
            _candidateTarget = null;
        }
	}

	public void EndAiCalculations () {
		OnTargetCalculatedEvent.Invoke ();
	}

	public bool IsActive {
		get {
			return gameObject.activeSelf;
		}
	}

	#endregion

	int IEntity.team {
		get {
			return team;
		}
	}

	public EntityHealth EntityHealth {
		get {
			return _entityHealth;
		}
	}
}

