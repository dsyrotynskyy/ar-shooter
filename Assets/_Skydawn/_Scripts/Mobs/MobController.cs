﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(AIMob))]
[RequireComponent(typeof(MobAnimatorController))]
[RequireComponent(typeof(EntityHealth))]
[RequireComponent(typeof(ArMobNavigationManager))]
public class MobController : MonoBehaviour, IEntity {
    
	PlayerAvatar _playerAvatar;
	MobAnimatorController _animatorController;
    ArMobNavigationManager _agent;
	public EntityHealth EntityHealth { get; private set;}
	Vector3 _spawnPosition;
	AIMob _aiMob;
    MobSoundManager _mobSoundManager;

	public AttackSpell attackSpell;

	void Awake () {
        _mobSoundManager = GetComponent<MobSoundManager>();
		_aiMob = GetComponent<AIMob> ();

		_aiMob.OnTargetCalculatedEvent.AddListener (OnTargetCalculated);
        _animatorController = GetComponent<MobAnimatorController>();
		EntityHealth = GetComponent<EntityHealth> ();
		_agent = GetComponent<ArMobNavigationManager>();
		_playerAvatar = PlayerAvatar.GetInstance;

        EntityHealth.OnTakenDamage.AddListener((float d, IEntity attacker)=> {
            this._mobSoundManager.OnDamaged();
        } );

		_animatorController.OnAttackEvent.AddListener (OnAttackAnimation);
		EntityHealth.OnDiedEvent.AddListener (OnDied);

 //       EntityHealth.maxHealth = GameConfig.GetInstance.PlayerHealth;
 //       attackPower = GameConfig.GetInstance.EnemyAttackPower;
    }

	void OnTargetCalculated () {
		if (_aiMob.AttackTarget != null) {
			AttackTarget = _aiMob.AttackTarget.EntityHealth;
		}
	}

	void Start () {
		_spawnPosition = transform.position;

		InvokeRepeating("UpdateLogic", 0.1f, 0.1f);

		UpdateLogic();

//		Invoke ("Kill", 1f);
	}

	void Kill () {
		EntityHealth.TakeDamage (EntityHealth.maxHealth, null);
	}

	void UpdateLogic () {
		if (!gameObject.activeInHierarchy) {
			return;
		}

		if (!IsAlive) {
			Debug.LogError ("WTF");
			return;
		}

		if (!_animatorController.IsPlayingAttackAnimation) {
			if (AttackTarget) {
				if (attackEnabled) {
					_agent.destination = AttackTarget.transform.position;
				} else {
					_agent.destination = transform.position;
				}
			} else {
				_agent.destination = _spawnPosition;
			}
        } else {
            _agent.destination = transform.position;
        }
	}

	void OnDied () {
        Collider[] colliders;
        colliders = GetComponents<Collider>();
        foreach (var c in colliders) {
            Destroy(c);
        }

		CancelInvoke ("UpdateLogic");
		enabled = false;
		_agent.isStopped = true;
        _agent.enabled = false;
        _animatorController.OrderDead();
	}

	public bool IsAlive {
		get {
			return this.EntityHealth.IsAlive;
		}
	}

    float _distanceToTarget;

    public float DistanceToTarget {
        get {
            return _distanceToTarget;
        }
    }

	public float rangeAttackRadius = 0f;
		
	void FixedUpdate () {
        if (!IsAlive) {
            return;
        }

        bool enoughCloseToAttack = false;
        if (AttackTarget && AttackTarget.IsAlive)
        {
            _distanceToTarget = Vector3.Distance(AttackTarget.transform.position, transform.position);
			float plus = 0f;
			if (IsAttackOrdered) {
				plus += 0.15f;
			}
			float targetRadius = 0f;
			if (AttackTarget.collider && AttackTarget.collider.enabled) {
				targetRadius += AttackTarget.collider.radius;
			}
			enoughCloseToAttack = _distanceToTarget < _agent.radius + _agent.stoppingDistance + plus + targetRadius + rangeAttackRadius;
        } else {
            _distanceToTarget = 0f;
        }

        float mag = _agent.velocity;
        bool shouldMove = mag > 0.9f;

        if (_animatorController.IsMovementOrdered) {
            shouldMove = mag > 0.5f;

            if (!shouldMove)
            {
                Debug.Log("Not should move! " + mag);
            }
        }

        if (mag > 4.5f) {
            enoughCloseToAttack = false;
        }

		bool wasAttackOrdered = IsAttackOrdered;

        if (!PlayerAvatar.GetInstance.InGame)
        {
            enoughCloseToAttack = false;
        }

		if (!attackEnabled) {
			enoughCloseToAttack = false;
		}

		if (wasAttackOrdered != enoughCloseToAttack) {
			IsAttackOrdered = enoughCloseToAttack;
        } 

		_animatorController.OrderAttack(enoughCloseToAttack);
        _animatorController.OrderMove(shouldMove);
        _animatorController.UpdateState();
	}

	bool attackOrderered = false;

	public bool IsAttackOrdered {
		get  {
			return attackOrderered;
			}
		set {
			if (attackOrderered != value) {
				attackOrderered = value;
				attackOrderedEvent.Invoke(attackOrderered);
			}
		}
	}

	public TemplateEvent<bool> attackOrderedEvent = new TemplateEvent<bool> ();

    public float attackPower = 20f;

	public Transform spellCastPosition;

	public float attackDelay = 0f;

	bool attackEnabled = true;

	void OnAttackAnimation () {
		if (!this.EntityHealth.IsAlive) {
			return;
		}

		if (this.rangeAttackRadius > 0f) {
			var spell = GameObject.Instantiate(attackSpell, spellCastPosition.position, new Quaternion());
			spell.onCollisionEvent.AddListener(OnSpellCollided);

			var e = spell.GetComponent<EffectSettings>();
        	e.Target = AttackTarget.gameObject;
		} else {
        	AttackTarget.TakeDamage (attackPower, this);
		}

		if (attackDelay > 0f) {
			attackEnabled = false;
			this.Invoke(attackDelay, ()=> 
			{
				this.attackEnabled = true;
				FixedUpdate();
			});
		}
	}

	public void OnSpellCollided (AttackSpell spell, Collider collider) {
        var entity = collider.gameObject.GetComponent<EntityHealth>();
        if (entity) {
            entity.TakeDamage(attackPower, null);
        }
    }

	EntityHealth _target;
	public EntityHealth AttackTarget { get { return _target; } 
		set {
			if (System.Object.ReferenceEquals(_target, value)) {
				return;
			}
			_target = value;
			if (_target) {
				_animatorController.target = _target.transform;
			} else {
				_animatorController.target = null;
				_distanceToTarget = -1f;
			}
		}
	}

	float IEntity.GetSqrDistance (IEntity entity)
	{
		return this._aiMob.GetSqrDistance (entity);
	}

	int IEntity.team {
		get {
			return this._aiMob.team;
		}
	}

	IEntity IEntity.AttackTarget { get { return _aiMob.AttackTarget; } }

    public Transform spellAttackAnchor { get { return _aiMob.spellAttackAnchor; } }
}
