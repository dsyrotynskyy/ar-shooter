using UnityEngine;
using System.Collections;

public class PlayerSoundManager : MonoBehaviour
{
    public AudioClip[] onDamagedSounds;
    public AudioClip[] missSounds;
    public AudioClip[] attackSounds;

    public void PlayAttackSound(bool damagedEnemy = false)
    {
        AudioClip s;
        if (damagedEnemy)
        {
            s = attackSounds.GetRandomElement<AudioClip>();
        }
        else
        {
            s = missSounds.GetRandomElement<AudioClip>();
        }

        var a = AudioPlayer.GetOrCreateInstance.Play(s);
        a.pitch = UnityEngine.Random.Range(0.9f, 1.1f);
    }

    public void OnDamaged() 
    { 
        AudioClip s;
        s = onDamagedSounds.GetRandomElement<AudioClip>();

        var a = AudioPlayer.GetOrCreateInstance.Play(s);
        a.pitch = UnityEngine.Random.Range(0.9f, 1.1f);
    }

}
