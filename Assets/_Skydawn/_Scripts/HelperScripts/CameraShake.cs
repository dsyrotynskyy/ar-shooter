﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class CameraShake : UnitySingleton<CameraShake>
{
    [HideInInspector]
    Transform camTransform;

    private float shakeTime = 0f;
    private float shakePower = 0.7f;

    public float decreaseFactor = 1.0f;

    Vector3 originalPos;

    protected override bool OrderDontDestroyOnLoad
    {
        get
        {
            return false;
        }
    }

    protected override void OnAwake()
    {
        base.OnAwake();
        if (camTransform == null)
        {
            camTransform = transform;
        }
        originalPos = camTransform.localPosition;
    }

    void Update()
    {
        if (shakeTime > 0 && _targetTime > 0)
        {
            Vector3 newPos = originalPos + UnityEngine.Random.insideUnitSphere * shakePower * Mathf.Sqrt(shakeTime) * shakeTime / _targetTime;
            LerpLocalPos(newPos, Time.deltaTime * 20f);
            shakeTime -= Time.deltaTime * decreaseFactor;
//            Debug.Log("Newpos = " + newPos);
        }
        else
        {
            shakeTime = 0f;
            LerpLocalPos(originalPos, Time.deltaTime * 20f);
            isDoingBigShake = false;
        }
    }

    void LerpLocalPos(Vector3 target, float step)
    {
        camTransform.localPosition = Vector3.Lerp(camTransform.localPosition, target, step);
    }

    float _targetTime;

    public void OrderShake(float newShakePower, float newShakeTime)
    {
        _targetTime = newShakeTime;

        this.shakePower = newShakePower;
        this.shakeTime = newShakeTime;
    }

    internal void PlaySmallShake()
    {
        if (isDoingBigShake) {
            return;
        }
        OrderShake(2.0f, 0.3f);
    }

    internal void PlayBiggerShake()
    {
		if (isDoingBigShake)
		{
			return;
		}
        OrderShake(2.0f, 0.3f);
    }

    bool isDoingBigShake = false;

    internal void PlayHeroDeathShake()
    {
        isDoingBigShake = true;
		OrderShake(2.0f, 1f);
    }
}

