﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Experimental.XR;
using System;
using UnityEngine.UI;
using UnityEngine.XR.ARSubsystems;

public class ARTapToPlaceObject : MonoBehaviour
{
    public GameObject objectToPlace;
    public GameObject placementIndicator;

    private ARSessionOrigin arOrigin;

    private ARRaycastManager aRRaycastManager;
    private Pose placementPose;
    private bool placementPoseIsValid = false;

    public Button tapButton;

    void Start()
    {
        arOrigin = FindObjectOfType<ARSessionOrigin>();
        aRRaycastManager = arOrigin.GetComponent<ARRaycastManager>();
        tapButton.onClick.AddListener(PlaceObject);
    }

    void LateUpdate()
    {
        if (Application.isEditor)
        {
            return;
        }
        UpdatePlacementPose();
        UpdatePlacementIndicator();

        //if (placementPoseIsValid)
        //{
        //    if (Input.GetMouseButtonDown(0))
        //    {
        //        this.Invoke("PlaceObject", 0.1f);
        //    }
        //}
    }

    private void UpdatePlacementPose()
    {
        var screenCenter = Camera.current.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        var hits = new List<ARRaycastHit>();
        aRRaycastManager.Raycast(screenCenter, hits, TrackableType.Planes);

        placementPoseIsValid = hits.Count > 0;
        Debug.Log("Pose is valid = " + placementPoseIsValid);
        if (placementPoseIsValid)
        {
            placementPose = hits[0].pose;

            var cameraForward = Camera.current.transform.forward;
            var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;
            placementPose.rotation = Quaternion.LookRotation(cameraBearing);
        }
    }

    private void UpdatePlacementIndicator()
    {
        if (placementPoseIsValid)
        {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
        else
        {
            placementIndicator.SetActive(false);
        }
    }

    public void PlaceObject()
    {
        if (!placementPoseIsValid)
        {
            return;
        }
        if (!this.gameObject.activeSelf)
        {
            return;
        }
        var res = Instantiate(objectToPlace, placementPose.position, placementPose.rotation);
        res.SetActive(true);

        res.transform.LookAt(PlayerAvatar.GetInstance.transform);
        var e = transform.eulerAngles;
        e.x = 0f;
        e.z = 0f;
        transform.eulerAngles = e;
    }
}
