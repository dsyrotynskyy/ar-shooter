﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UTextKills : MonoBehaviour
{
    public Text text;
    AIManager AIManager {get;set;}
    void Start()
    {
        AIManager = AIManager.GetInstance;
        this.InvokeRepeating("UpdateUI", 0f, 1f);
    }

    void UpdateUI()
    {
        int dead = AIManager.CountDeadMobs();
        if (dead > 0) {
            text.text = dead + "";
        } else {
            text.text = "";
        }
    }
}
