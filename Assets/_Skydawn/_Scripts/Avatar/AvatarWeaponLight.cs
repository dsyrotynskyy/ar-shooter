﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarWeaponLight : MonoBehaviour {

    public Light Light;

	void Start () {
        Light.color = RenderSettings.ambientLight;
	}
}
