﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UTestMenu : MonoBehaviour {

    public string gameSceneName = "_AR";

    public Button startGameButton;
//    public Button buttonResetAll;

    public InputField playerHPInputField;
    public InputField enemyRadiusInputField;
    public InputField enemyAttackPowerInputField;
    public InputField playerRadiusInputField;
    public InputField attackTypeInputField;

    GameConfig _gameConfig;

    void Start () {
        _gameConfig = GameConfig.GetInstance;

//        buttonResetAll.onClick.AddListener(ResetProgress);
        startGameButton.onClick.AddListener(OnStartButtonClick);

        UpdateUI();
	}

    void UpdateUI () {

        playerHPInputField.text = _gameConfig.PlayerHealth.ToString();

        enemyRadiusInputField.text = _gameConfig.EnemyRadius.ToString();

        enemyAttackPowerInputField.text = _gameConfig.EnemyAttackPower.ToString();

        playerRadiusInputField.text = _gameConfig.PlayerAttackRadius.ToString();

        attackTypeInputField.text = _gameConfig.PlayerAttackType.ToString();
    }

    //private void ResetProgress()
    //{
    //    UpdateUI();
    //}

    private void OnStartButtonClick()
    {
        SceneManager.LoadScene(gameSceneName);

        int enemyHP = int.Parse(playerHPInputField.text);
        _gameConfig.PlayerHealth = enemyHP;

        float enemyRadius = float.Parse(enemyRadiusInputField.text);
        _gameConfig.EnemyRadius = enemyRadius;

        float enemyAttackPower = float.Parse(enemyAttackPowerInputField.text);
        _gameConfig.EnemyAttackPower = enemyAttackPower;

        float playerRadius = float.Parse(playerRadiusInputField.text);
        _gameConfig.PlayerAttackRadius = playerRadius;

        int attackType = int.Parse(attackTypeInputField.text);
        _gameConfig.PlayerAttackType = attackType;
    }
}
