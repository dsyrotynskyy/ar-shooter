﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class USwitchScene : MonoBehaviour {

    public Button myButton;

	void Start () {
        myButton.onClick.AddListener(ButtonClick_SwitchScene);
	}

	void ButtonClick_SwitchScene () {
        SceneController.GetInstance.SwitchScene();
	}
}
