using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.CrossPlatformInput;
using System;
using UnityEngine.Events;

public class AvatarAttackController : MonoBehaviour
{
    public Animator Animator;

    public float maxTime = 0.3f;
    float _actionTime = 0;
    const string attackAnimationStateName = "attack";

	public void OnAvatarDied () {
		enabled = false;
		Animator.SetTrigger ("dead");
	}

    void Update()
    {
        TriggerAttack(animationMode);



//        bool isJump = CrossPlatformInputManager.GetButtonDown("Jump");
//        if (isJump)
//        {
//            _actionTime -= maxTime;
//        }

//        if (Input.touchCount > 0)
//        {
//            for (int i = 0; i < Input.touchCount; i++)
//            {
//                var t = Input.GetTouch(i);

//                if (t.phase == TouchPhase.Began)
//                {
//                    _actionTime = Time.realtimeSinceStartup;
//                    break;
//                }

//                if (t.deltaPosition.sqrMagnitude < 10f && t.phase == TouchPhase.Ended)
//                {
//                    float curTime = Time.realtimeSinceStartup;
//                    if (curTime - _actionTime < maxTime)
//                    {
//                        TriggerAttack(AnimationMode.Attack);
//                    }
//                    break;
//                }
//            }
//        }

//        #if UNITY_EDITOR
//        if (Input.GetMouseButtonDown(0)) {
//            _actionTime = Time.realtimeSinceStartup;
//        }

//        if (Input.GetMouseButtonUp (0)) {
//            float curTime = Time.realtimeSinceStartup;
//            if (curTime - _actionTime < maxTime) {
//                TriggerAttack(AnimationMode.Attack);
//            }
//        }
//#endif
	}

	void OnAttackTargetChanged () {
	}

    AnimationMode animationMode = AnimationMode.Idle;

    internal void SetAnimationMode(AnimationMode v)
    {
        animationMode = v;
    }

    public enum AnimationMode {Idle, Attack}
    public bool RageEnabled;

    void TriggerAttack (AnimationMode animationMode) {
        //if (PlayerAvatar.GetInstance.AttackTarget == null)
        //{
        //    Animator.SetInteger("attack", 0);
        //    return;
        //}

        if (animationMode == AnimationMode.Idle)
        {
            if (RageEnabled) {
                Animator.SetInteger("attack", 0);
            }
            return;
        }

        string targetState;

        int id;
        if (RageEnabled) {
            targetState = "_aweapon rage";
            id = 4;
        } else {
            id = UnityEngine.Random.Range(0, 3) + 1;
            targetState = "_pweapon attack" + id;
        }

//        Debug.Log("Animator play: " + targetState);

        Animator.SetInteger("attack", id);
    }

    public void Animator_OnAttackPerformed () {
//		Debug.LogError ("asdf");
        Animator.SetInteger(attackAnimationStateName, 0);

        OnAttackPerformedEvent.Invoke();
    }

    public void Animator_OnRageAttackEnded () {
        OnRageAttackEndedEvent.Invoke();
    }

	void PlayAttackSound () {
	}

    public UnityEvent OnRageAttackEndedEvent = new UnityEvent();
    public UnityEvent OnAttackPerformedEvent = new UnityEvent();

    public bool TargetSelected { get; internal set; }
}
