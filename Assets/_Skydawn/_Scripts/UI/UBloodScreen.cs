﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UBloodScreen : MonoBehaviour {

	public float bloodAplhaPower = 0.3f;
	public Image bloodImage;
	// Use this for initialization
	void Start () {
		PlayerAvatar _playerAvatar = PlayerAvatar.GetInstance;
		var e = _playerAvatar.EntityHealth;
		e.OnTakenDamage.AddListener (OnTakenDamage);
		e.OnDiedEvent.AddListener (OnDied);

		var c = bloodImage.color;
		c.a = 0f;
		bloodImage.color = c;
	}

	bool _isDead = false;
	
	void OnTakenDamage (float damage, IEntity e) {
		DisplayBlood ();
	}

	void OnDied () {
		_isDead = true;
		DisplayBlood ();
	}

	void DisplayBlood () {
		var c = bloodImage.color;
		c.a = bloodAplhaPower;
		bloodImage.color = c;
	}

	public float bloodFadeSpeed = 3f;

	void FixedUpdate () {
		if (_isDead) {
			return;
		}
		var c = bloodImage.color;
        c.a = Mathf.Lerp (c.a, 0f, Time.fixedDeltaTime * bloodFadeSpeed);
		bloodImage.color = c;
	}
}
