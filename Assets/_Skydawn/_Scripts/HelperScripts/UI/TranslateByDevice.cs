﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateByDevice : MonoBehaviour {
	public Vector3 iphoneXTranslate= Vector3.zero;

	void Start () {
		if (DeviceHelper.IsIPhoneX) {
			transform.localPosition += iphoneXTranslate;
		} 
	}
}
