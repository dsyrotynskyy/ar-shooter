﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EntityHealth : MonoBehaviour {
	public CapsuleCollider collider;

    public bool lifeRestoreEnabled = false;
	public float restoreHealthPercent = 0.03f;
	public bool IsImmortal = false;
	float _currentHealth;
	public float currentHealth { get { return _currentHealth;} 
		private set { 
			if (Mathf.Abs (_currentHealth - value) > 0) {
				_currentHealth = value;	
				OnHealthChangedEvent.Invoke ();
			}
		}
	}
	public float maxHealth = 100;
	internal UnityEvent OnDiedEvent = new UnityEvent();
	internal UnityEvent OnHealthChangedEvent = new UnityEvent ();
	internal TakenDamageEvent OnTakenDamage = new TakenDamageEvent ();
	public class TakenDamageEvent : UnityEvent <float, IEntity> {}

	bool _alive = false;

	void Awake () {
		_alive = true;
        _currentHealth = maxHealth;
	}

	float _lastTimeAttacked = 0f;

	public bool TakeDamage (float damage, IEntity attacker) {
		if (IsImmortal) {
			return true;
		}
        if (!_alive) {
            return false;
        }

		_lastTimeAttacked = Time.realtimeSinceStartup;

		currentHealth -= damage;
		currentHealth = Mathf.Clamp (currentHealth, 0f, maxHealth);
		OnTakenDamage.Invoke (damage, attacker);
		if (currentHealth <= 0f) {
			_alive = false;
			OnDiedEvent.Invoke ();
		}
        return true;
	}

	void FixedUpdate () {
        if (!lifeRestoreEnabled) {
            return;
        }

		float healthRestoreBattleDelay = 2f;
        if (Time.realtimeSinceStartup - _lastTimeAttacked < healthRestoreBattleDelay) {
			return;
		}

		if (_alive) {
			float restoreHealthValue = restoreHealthPercent * maxHealth * Time.fixedDeltaTime;
			currentHealth += restoreHealthValue;
			currentHealth = Mathf.Clamp (currentHealth, 0f, maxHealth);
		}
	}

	public float HealthPercent {
		get { 
			return currentHealth / maxHealth;
		}
	}

	public bool IsAlive {
		get {
			return _alive;
		}	
	}
}
