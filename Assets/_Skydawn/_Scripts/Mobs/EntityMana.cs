﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EntityMana : MonoBehaviour
{
    public bool restoreEnabled = true;
//    public float restoreManaPercent = 0.03f;
    float _currentMana;
    public float currentMana
    {
        get { return _currentMana; }
        private set
        {
            if (Mathf.Abs(_currentMana - value) > 0)
            {
                _currentMana = value;
                OnManaChangedEvent.Invoke();
            }
        }
    }
    public float maxMana = 100;
    internal UnityEvent OnEndedEvent = new UnityEvent();
    internal UnityEvent OnManaChangedEvent = new UnityEvent();
//    internal ManaUsedEvent OnManaUsedEvent = new ManaUsedEvent();
    public class ManaUsedEvent : UnityEvent<float, IEntity> { }

//    bool _alive = false;

    void Awake()
    {
//        _alive = true;
        _currentMana = maxMana;
    }

    float _lastTimeAttacked = 0f;

    public bool UseMana(float damage)
    {
        if (currentMana - damage < 0f)
        {
            return false;
        }
        _lastTimeAttacked = Time.realtimeSinceStartup;

        currentMana -= damage;
        currentMana = Mathf.Clamp(currentMana, 0f, maxMana);
//        OnManaUsedEvent.Invoke(damage, attacker);
        if (currentMana <= 0f)
        {
            OnEndedEvent.Invoke();
        }
        return true;
    }

    private void FixedUpdate()
    {
        if (!restoreEnabled)
        {
            return;
        }
        float restoreHealthValue = GameConfig.GetInstance.restoreManaPercent * maxMana * Time.fixedDeltaTime;
        currentMana += restoreHealthValue;
        currentMana = Mathf.Clamp(currentMana, 0f, maxMana);
    }

    public float ManaPercent
    {
        get
        {
            return currentMana / maxMana;
        }
    }

    //public bool IsAlive
    //{
    //    get
    //    {
    //        return _alive;
    //    }
    //}
}
