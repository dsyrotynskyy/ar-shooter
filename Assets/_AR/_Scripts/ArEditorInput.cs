﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArEditorInput : MonoBehaviour
{
#if UNITY_EDITOR
    float speed = 1f;

    void Update()
    {
        Vector3 add = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            add.z += 1f;
        }

        if (Input.GetKey(KeyCode.S))
        {
            add.z -= 1f;
        }

        if (Input.GetKey(KeyCode.A))
        {
            add.x -= 1f;
        }

        if (Input.GetKey(KeyCode.D))
        {
            add.x += 1f;
        }

        transform.Translate(add * speed * Time.unscaledDeltaTime);
    }
#endif

}
