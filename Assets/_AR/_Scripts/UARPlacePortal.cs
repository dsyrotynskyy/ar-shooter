﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UARPlacePortal : MonoBehaviour
{
    public Text PlacePortalText;
    public Text OpenPortalText;
    public ARPortalSpawner portalSpawner;
    public Button placePortalButton;

    EnemyPortal placedPortal;

    void Start()
    {
        placePortalButton.onClick.AddListener(PlacePortal);

        PlacePortalText.gameObject.SetActive(true);
        OpenPortalText.gameObject.SetActive(false);
    }

    void FixedUpdate()
    {
        if (portalSpawner.PlacementPoseIsValid) {
            PlacePortalText.text = "Place Portal!";
        } else {
            PlacePortalText.text = "Seeking place for Portal...";
        }

    }

    private void PlacePortal()
    {
        if (!placedPortal) {
            Debug.Log("UI: Place Enemy Spawner");
            placedPortal = portalSpawner.PlaceObject();

            PlacePortalText.gameObject.SetActive(false);
            OpenPortalText.gameObject.SetActive(true);
        } else {
            placedPortal.OpenPortal();
            gameObject.SetActive(false);
        }
    }
}
