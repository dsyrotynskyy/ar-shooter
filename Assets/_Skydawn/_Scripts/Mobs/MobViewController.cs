﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EntityHealth)) ]
public class MobViewController : MonoBehaviour {

	public Transform bloodPoint;
//	public EntityHealth myEntity;
	public GameObject bloodParticles;

	void Start () {
		EntityHealth e = GetComponent<EntityHealth> ();
		e.OnTakenDamage.AddListener (OnTakenDamage);
	}

	void OnTakenDamage (float damage, IEntity e) {
		Quaternion q = new Quaternion ();
		Vector3 p = transform.position;

		if (e != null) {
			var f = e.transform.position - p;
			q = Quaternion.LookRotation (f);
		}

		if (bloodPoint != null) {
			p = bloodPoint.transform.position;
		}

		var g = Instantiate (bloodParticles, p, q);
//		Destroy (g, 10f);

	}
}
