﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityStandardAssets.Characters.FirstPerson;

[RequireComponent(typeof(EntityHealth))]
public class PlayerAvatar : UnitySingleton<PlayerAvatar>, IEntity {
	public int team = -1;

	int IEntity.team {
		get {
			return team;
		}
	}

    public Transform spellAttackAnchor { get { return transform; } }

    public AvatarAttackController avatarAttackController;
	public Animator Animator;
	public EntityHealth EntityHealth { get; private set;}
	public AvatarInteractionPoint interactionPoint;
    public AvatarInteractionPoint magicInteractionPoint;
    PlayerSoundManager _playerSoundManager;
    SwipeRecognizer swipeRecognizer;

    GameConfig GameConfig { get; set; }

	void Awake () {
        GameConfig = GameConfig.GetInstance;
        _playerSoundManager = GetComponent<PlayerSoundManager>();
		EntityHealth = GetComponent<EntityHealth> ();
        EntityHealth.maxHealth = GameConfig.GetInstance.PlayerHealth;
        avatarAttackController.OnAttackPerformedEvent.AddListener(OnAttackAnimationPerformed);
        avatarAttackController.OnRageAttackEndedEvent.AddListener(OnRageAttackAnimationPerformed);
		interactionPoint.mobEnteredEvent.AddListener (RegisterPlayerDamager);
		interactionPoint.mobExitEvent.AddListener (UnregisterPlayerDamager);
        swipeRecognizer = gameObject.AddComponent<SwipeRecognizer>();
        swipeRecognizer.OnSwipeInchesContinue += SwipeRecognizer_OnSwipeInchesContinue;

//		Invoke ("KillPlayer", 1f);
        InvokeRepeating("CheckAttackTargets", 0.1f, 0.1f);

        EntityHealth.OnTakenDamage.AddListener((float d, IEntity attacker) => {
            this._playerSoundManager.OnDamaged();
        });
	}

    public float attackDamage = 20f;

    void Start() {
		EntityHealth.OnDiedEvent.AddListener (OnDie);
	}

// 	public void TakeDamage (float damage, IEntity attacker) {
// 		EntityHealth.TakeDamage (damage, attacker);
// //        _playerSoundManager.OnDamaged();
// 	}

	public void KillPlayer () {
		EntityHealth.TakeDamage (EntityHealth.maxHealth, null);
	}

	void OnDie () {
        avatarAttackController.OnAvatarDied();
		Animator.SetTrigger ("dead");

        CancelInvoke("CheckAttackTargets");
	}

	public bool IsAlive {
		get {
			return this.EntityHealth.IsAlive;
		}
	}

    HashSet<IEntity> _playerDamagers = new HashSet<IEntity>();

	private void RegisterPlayerDamager (IEntity damager) {
        if (!_playerDamagers.Contains(damager)) {
//            Debug.Log("Add attacker");
            _playerDamagers.Add(damager);
            CheckAttackTargets();
        }
	}

	private void UnregisterPlayerDamager (IEntity damager) {
        if (_playerDamagers.Contains(damager))
        {
//            Debug.Log("Remove attacker");
            _playerDamagers.Remove(damager);

            CheckAttackTargets();
        }
	}

	List<IEntity> attackableMobs = new List<IEntity> ();

    private bool CheckAttackTargets()
    {
		if (AttackTarget != null && CanAttack (AttackTarget)) {
			return true;
		}

        foreach (var t in _playerDamagers) {
			if (CanAttack (t)) {
				AttackTarget = t;
                _targetSelected = true;
                if (GameConfig.IsAutoAttack)
                {
                    avatarAttackController.SetAnimationMode(AvatarAttackController.AnimationMode.Attack);
                }

				AttackTargetChangedEvent.Invoke ();
				return true;
			}
        }
		if (AttackTarget != null) {
			AttackTarget = null;
            _targetSelected = false;
            if (GameConfig.IsAutoAttack)
            {
                avatarAttackController.SetAnimationMode(AvatarAttackController.AnimationMode.Idle);
            }

            AttackTargetChangedEvent.Invoke();
		}

        return false;
    }

    public bool WeaponEnabled = false;

    void LateUpdate()
    {
        this.Animator.gameObject.SetActive(InGame && WeaponEnabled);
        if (GameConfig.IsTouchAttack)
        {
            //if (AttackEnabled && Input.GetMouseButtonDown(0))
            //{
            //    OrderAttackMellee();
            //}
        }
    }

    public void OrderAttackMellee ()
    {
        avatarAttackController.SetAnimationMode(AvatarAttackController.AnimationMode.Attack);
    }

    void SwipeRecognizer_OnSwipeInchesContinue(Vector2 arg1, Direction arg2)
    {
        if (!GameConfig.IsSwipeAttack)
        {
            return;
        }

        if (arg1.magnitude > 1f)
        {
            avatarAttackController.SetAnimationMode(AvatarAttackController.AnimationMode.Attack);
        }
    }

    bool _targetSelected = false;

    private void OnRageAttackAnimationPerformed()
    {
        RagePower = 0f;
    }

	private void OnAttackAnimationPerformed()
	{
        if (!GameConfig.IsAutoAttack)
        {
            avatarAttackController.SetAnimationMode(AvatarAttackController.AnimationMode.Idle);
        }

        if (AttackTarget != null) {
            //			Debug.Log ("Attack enemy");

            float actualDamage = attackDamage;
            if (IsRage) {
                actualDamage *= 1.5f;
            }

            bool damaged = AttackTarget.EntityHealth.TakeDamage(actualDamage, this);
            if (damaged) {
                RagePower += 0.12f;
            }
//			Debug.LogError ("asdf");
			if (!AttackTarget.EntityHealth.IsAlive) {
				_playerDamagers.Remove (AttackTarget);
				CheckAttackTargets ();
			}

            _playerSoundManager.PlayAttackSound(true);
        } else {
            _playerSoundManager.PlayAttackSound(false);
        }
	}

    private void FixedUpdate()
    {
        if (!IsRage)
        {
            RagePower = Mathf.Lerp(RagePower, 0f, Time.fixedDeltaTime * 0.01f);
        }
        avatarAttackController.RageEnabled = IsRage;
        if (AttackTarget == null) {
            _targetSelected = false;
        }
        avatarAttackController.TargetSelected = _targetSelected;
    }

    float _ragePower = 0f;
    public float RagePower
    {
        get
        {
            return _ragePower;
        }
        set
        {
            _ragePower = value;
            OnRageValueChangedEvent.Invoke();
        }
    }
    internal UnityEvent OnRageValueChangedEvent = new UnityEvent();

    public bool IsRage {
        get {
            return _ragePower >= 1f;
        }         
    }

    bool CanAttack (IEntity playerDamager) {
		if (!playerDamager.EntityHealth.IsAlive) {
			return false;
		}
//		return true;

		Vector3 offset = playerDamager.transform.position - interactionPoint.transform.position;
		offset.y = 0f;
		if (offset.magnitude < 2f) {
			return true;
		} else {
			return false;
		}
	}

    internal UnityEvent AttackTargetChangedEvent = new UnityEvent();
    public IEntity AttackTarget;

	public float GetSqrDistance (IEntity entity)
	{
		throw new NotSupportedException ();
	}

	IEntity IEntity.AttackTarget {
		get {
			return AttackTarget;
		}
	}

    public bool _attackEnabled = false;

    public bool InGame {
        get {
            return _attackEnabled;
        }
        set {
            _attackEnabled = value;
            AttackEnabledChangedEvent.Invoke();
        }
    }

    public UnityEvent AttackEnabledChangedEvent = new UnityEvent();
}
