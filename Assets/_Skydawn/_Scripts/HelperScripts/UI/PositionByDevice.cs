﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionByDevice : MonoBehaviour {
	public bool isLocal;
	public Vector3 normalPosition = Vector3.zero;
	public Vector3 iphoneXPosition= Vector3.zero;

	void Start () {
		if (isLocal) {
			if (DeviceHelper.IsIPhoneX) {
				transform.localPosition = iphoneXPosition;
			} else {
				transform.localPosition = normalPosition;
			}
		} else {
			if (DeviceHelper.IsIPhoneX) {
				transform.position = iphoneXPosition;
			} else {
				transform.position = normalPosition;
			}
		}
	}
}
