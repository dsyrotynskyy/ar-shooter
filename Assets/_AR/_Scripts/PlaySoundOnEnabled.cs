﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnEnabled : MonoBehaviour
{
    public AudioClip audioClip;

    public bool isLoop;

    public float volume = 1f;

    void Start()
    {
        AudioSource res;
        if (isLoop) {
             res = AudioPlayer.GetOrCreateInstance.PlayLoop (audioClip);
        } else {
            res = AudioPlayer.GetOrCreateInstance.Play(audioClip);
        }
        res.volume = this.volume;
    }
}
