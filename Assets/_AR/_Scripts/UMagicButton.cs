﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UMagicButton : MonoBehaviour
{
    public Button button;
    public AvatarMagicSpawner avatarMagicSpawner;

    void Start()
    {
        button.onClick.AddListener(OnButtonClick);
    }

    private void OnButtonClick()
    {
        avatarMagicSpawner.Spawn();
    }
}
