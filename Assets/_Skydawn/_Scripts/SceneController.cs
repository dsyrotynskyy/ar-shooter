﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : UnitySingleton<SceneController> {

    public SceneType CurrentScene;
    public enum SceneType {Armageddon, Forest}

    public void SwitchScene () {
        string targetScene;
        if (CurrentScene == SceneType.Armageddon) {
            targetScene = "_Forest";
        } else {
            targetScene = "_Armageddon";
        }

        SceneManager.LoadScene(targetScene);
    }
}
