﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UPlayerTargetHealthBar : MonoBehaviour {

    public Image background;
    public Image foreground;

    EntityHealth _hp;
    PlayerAvatar _player;
    void Start()
    {
        _player = PlayerAvatar.GetInstance;
        _player.AttackTargetChangedEvent.AddListener(UpdateTarget);

//        _hp = _player.EntityHealth;
//        _hp.OnHealthChangedEvent.AddListener(UpdateUI);

        UpdateTarget();
    }

    void UpdateTarget () {
        var a = _player.AttackTarget;
        if (a != null) {
            _hp = a.EntityHealth;
        } else {
            _hp = null;
        }

        background.gameObject.SetActive(_hp);
        foreground.gameObject.SetActive(_hp);
        UpdateUI();
    }

 //   float _targetHP = 1f;

    void UpdateUI()
    {
//		Debug.LogError ("asdf");
        if (_hp) {
 //           _targetHP = _hp.HealthPercent;
            foreground.fillAmount = _hp.HealthPercent;
        } 
    }

    void FixedUpdate()
    {
		UpdateUI ();
//        foreground.fillAmount = Mathf.Lerp(foreground.fillAmount, _targetHP, Time.deltaTime * 10f);
    }
}
