﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Audio;
using System;

public class GameSceneMusicManager : UnitySingleton<GameSceneMusicManager> {

    public AudioMixerGroup themeAudioMixer;
    public AudioMixerGroup eventMusicMixer;

    public AudioClip theme;
	public AudioClip themeBoss;
	public AudioClip themeHard;
	public AudioClip win;
	public AudioClip failure;

    AudioSource _eventSoundAudioSource;
    AudioSource _themeSound;

	protected override void OnAwake () {
//		LogHelper.Log("GameSceneMusicManager: start init");

        var v = AudioPlayer.GetOrCreateInstance.PlayLoop(theme);
        v.loop = true;
        v.transform.SetParent(this.transform);
        v.outputAudioMixerGroup = themeAudioMixer;
        _themeSound = v;

		PlayTheme ();

//		LogHelper.Log("GameSceneMusicManager: load finished");
	}

	//void Start () {
	//	var a = ApplicationManager.GetInstance.Ads;
 //       a.FireOnAdShow (OnAdShow);
 //       a.FireOnAdHidden (OnAdHidden);
	//}

    private void OnAdShow()
    {
        gameObject.SetActive(false);
        //        SoundTo(_themeSound, 0f);
    }

    private void OnAdHidden()
    {
        gameObject.SetActive(true);
//        SoundTo(_themeSound, 1f);
    }

    protected override void OnSceneLoaded(UnityEngine.SceneManagement.Scene arg0, UnityEngine.SceneManagement.LoadSceneMode arg1)
    {
        base.OnSceneLoaded(arg0, arg1);

		//if (SceneController.GetInstance.IsGameScene) {
		//	UnityEngine.XR.WSA.WorldManager.GetInstance.FireOnGameLaunched (PlayGameSceneMusic);
		//} else {
		//	PlayTheme();
		//}
    }

	//void PlayGameSceneMusic () {
	//	var d = DungeonController.GetInstance;
	//	if (d.FloorController.LevelType == LevelType.KillBoss) {
	//		SoundTo(_themeSound, 0f);
	//	} else {
	//		if (d.CurrentDungeon.IsHard) {
	//			PlayTheme (themeHard);
	//		} else {
	//			PlayTheme ();
	//		}
	//	}
	//}

	public void PlayBossTheme () {
		PlayTheme (themeBoss);
	}

    protected override bool OrderDontDestroyOnLoad
    {
        get
        {
            return true;
        }
    }

	public void PlayCurrentTheme (float delay = 0f) {
        if (_eventSoundAudioSource)
        {
            FadeAndDestroySound(_eventSoundAudioSource);
        }
        if (delay > 0f) {
            this.Invoke(delay, ()=> {
                PlayTheme(_themeSound.clip);
            });
        } else {
            PlayTheme(_themeSound.clip);
        }
	}

	public void PlayTheme () {
		PlayTheme (theme);
	}

	public void PlayTheme (AudioClip newAudioClip) {
		if (_eventSoundAudioSource)
		{
			FadeAndDestroySound(_eventSoundAudioSource);
		}

		float soundSwitchDelay = 0f;

		if (_themeSound.clip != newAudioClip) {
			this.SoundTo(_themeSound, 0f);
			soundSwitchDelay = SoundFadeDelay;
		}

		Action action = () => {
			if (_themeSound.clip != newAudioClip) {
				_themeSound.clip = newAudioClip;
				_themeSound.Play();
			}
			if (_themeSound.volume <= 0f) {
				this.SoundTo (_themeSound, 1f);
			}
		};

		if (soundSwitchDelay > 0f) {
			this.Invoke (soundSwitchDelay, action);
		} else {
			action ();
		}

	}

	public void PlayDeath () {
        PlayEventSound(failure);
	}

    public void PlayVictory () {
        PlayEventSound(win);
    }

    void PlayEventSound (AudioClip audioClip) {
		SoundTo(_themeSound, 0f);
		if (_eventSoundAudioSource)
		{
			FadeAndDestroySound(_eventSoundAudioSource);
		}

        SoundTo(_themeSound, 0f);
        _eventSoundAudioSource = AudioPlayer.GetOrCreateInstance.Play(audioClip);
        _eventSoundAudioSource.outputAudioMixerGroup = eventMusicMixer;
    }

	float SoundFadeDelay = 0.1f;

    void SoundTo (AudioSource source, float targetVolume) {
		DOTween.To(() => source.volume, x => source.volume = x, targetVolume, SoundFadeDelay);
    }

	void FadeAndDestroySound (AudioSource source) {
		var t = DOTween.To (() => source.volume, x => source.volume = x, 0f, SoundFadeDelay);
		t.onKill += () => {Destroy(source.gameObject);};
	}
}
