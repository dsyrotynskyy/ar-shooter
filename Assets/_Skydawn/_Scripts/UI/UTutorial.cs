﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UTutorial : MonoBehaviour
{
    public ARPortalSpawner portalSpawner;
    public GameObject fireTutorialText;
    public GameObject moveToDodgeEnemyBulletText;
    public Button axeButton;
    public Button magicAttackButton;

    enum ETutorialState { GameStarted = 0, FireOrdered = 1, DodgeOrdered = 2 };

    ETutorialState TutorialState = ETutorialState.GameStarted;

    void Start()
    {
        portalSpawner.onPortalSpawnedEvent.AddListener(OnPortalSpawned);

        PlayerAvatar.GetInstance.WeaponEnabled = false;
    }

    EnemyPortal EnemyPortal;

    private void OnPortalSpawned(EnemyPortal portal)
    {
        EnemyPortal = portal;
        EnemyPortal.onEnemySpawnedEvent.AddListener(OnEnemySpawned);
    }

    private void OnEnemySpawned(IEntity entity)
    {
        if (EnemyPortal.spawnedList.Count == 1)
        {
            if (this.TutorialState == ETutorialState.GameStarted)
            {
                this.TutorialState = ETutorialState.DodgeOrdered;

                this.TutorialState = ETutorialState.DodgeOrdered;
                dodgeTutorialMob = entity.transform.GetComponent<MobController>();
                dodgeTutorialMob.attackOrderedEvent.AddListener(DisplayDodgeTutorial);
            }
        }
    }

    void DisplayFireTutorial()
    {
        Time.timeScale = 0f;
        fireTutorialText.gameObject.SetActive(true);
        magicAttackButton.onClick.AddListener(OnFireButtonClick);
        PlayerAvatar.GetInstance.WeaponEnabled = true;
    }

    private void OnFireButtonClick()
    {
        Time.timeScale = 1f;
        fireTutorialText.gameObject.SetActive(false);
        magicAttackButton.onClick.RemoveListener(OnFireButtonClick);
    }

    private void Update()
    {
        CheckHideDodgeTutorial();
    }

    bool _moveToDodgeEnemyDisplayed;
    bool _moveToDodgeEnemyHidden;
    public float dodgeTutorialHideDistance = 0.5f;

    Vector3 _dodgeTutorialStartPosition;

    MobController dodgeTutorialMob;

    void DisplayDodgeTutorial(bool isAttack)
    {
        this.Invoke(1.5f, () =>
        {
            if (isAttack)
            {
                _moveToDodgeEnemyDisplayed = true;
                _dodgeTutorialStartPosition = PlayerAvatar.GetInstance.transform.position;
                moveToDodgeEnemyBulletText.SetActive(true);
                Time.timeScale = 0f;
                dodgeTutorialMob.attackOrderedEvent.RemoveListener(DisplayDodgeTutorial);
            }
        });
    }

    void HideDodgeTutorial()
    {
        moveToDodgeEnemyBulletText.SetActive(false);
        Time.timeScale = 1f;

        if (this.TutorialState == ETutorialState.DodgeOrdered)
        {
            this.TutorialState = ETutorialState.FireOrdered;

            this.Invoke(1f, () =>
            {
                DisplayFireTutorial();
            });
        }
    }

    void CheckHideDodgeTutorial()
    {
        if (!_moveToDodgeEnemyDisplayed)
        {
            return;
        }
        if (_moveToDodgeEnemyHidden)
        {
            return;
        }
        var offset = _dodgeTutorialStartPosition - PlayerAvatar.GetInstance.transform.position;
        var sqr = offset.magnitude;
        if (sqr > dodgeTutorialHideDistance)
        {
            _moveToDodgeEnemyHidden = true;
            HideDodgeTutorial();
        }
    }
}
