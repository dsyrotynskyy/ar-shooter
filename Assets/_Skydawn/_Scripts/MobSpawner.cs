﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MobSpawner : MonoBehaviour {
    
    public float radius = 5f;
    public int numObjects = 10;
    public MobController prefab;

    void Start()
    {
        for (int i = 0; i < numObjects; i++)
        {
            Spawn();
        }
    }

    void Spawn () {
        Vector3 center = transform.position;
        Vector3 newPos = center + new Vector3(Random.Range(-radius, radius), 0, Random.Range(-radius, radius));
//        newPos = center;
        Quaternion rot = new Quaternion();

//        Vector3 randomPoint = newPos + Random.insideUnitSphere * radius;
        NavMeshHit hit;
        if (NavMesh.SamplePosition(newPos, out hit, 1.0f, NavMesh.AllAreas))
        {
            Vector3 result = hit.position;
            var g = Instantiate(prefab, result, rot);
            g.gameObject.SetActive(false);
            g.gameObject.SetActive(true);
        }
    }

}
