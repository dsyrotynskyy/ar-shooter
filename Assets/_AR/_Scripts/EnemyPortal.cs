﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPortal : MonoBehaviour
{
    public int firstWaveEnemiesNum = 3;
    public GameObject firstWaveEnemyPrefab;

    public int secondWaveEnemiesNum = 3;

    public GameObject secondWaveEnemyPrefab;
    public PlaySoundOnEnabled portalMusic;
    public GameObject[] enemyPrefabs;
    public float SpawnPeriod = 30f;

    public List<IEntity> spawnedList = new List<IEntity>();
    void Start()
    {
        //        OpenPortal();
    }

    bool SpawnEnabled = false;

    public void OpenPortal()
    {
        SpawnEnabled = true;
        if (portalMusic)
        {
            portalMusic.enabled = true;
        }

        this.Invoke(SpawnPeriod, SpawnEnemy);

        PlayerAvatar.GetInstance.InGame = true;
    }

    void FixedUpdate()
    {
        if (SpawnEnabled)
        {
            CheckSpawn();
        }
    }

    void CheckSpawn()
    {
        if (spawnedList.Count <= 0)
        {
            SpawnEnemy();
        }

        if (zeroCheckEnabled)
        {
            int aliveCount = 0;

            foreach (var e in spawnedList)
            {
                if (e.EntityHealth.IsAlive)
                {
                    aliveCount++;
                }
            }

            if (aliveCount <= 0)
            {
                zeroCheckEnabled = false;
                this.Invoke(2f, SpawnEnemy);
                // if (spawnedList.Count > firstWaveEnemiesNum + secondWaveEnemiesNum)
                // {
                //     this.Invoke(1f, SpawnEnemy);
                // }
            }
        }
    }

    bool zeroCheckEnabled = true;

    GameObject GetNextEnemyPrefab()
    {
        int enemiesNum = spawnedList.Count;
        if (enemiesNum < firstWaveEnemiesNum)
        {
            return firstWaveEnemyPrefab;
        }

        if (enemiesNum < firstWaveEnemiesNum + secondWaveEnemiesNum)
        {
            return secondWaveEnemyPrefab;
        }
        return enemyPrefabs.GetRandomElement<GameObject>();
    }

    void SpawnEnemy()
    {
        zeroCheckEnabled = true;

        var spawnPos = transform.position;
        spawnPos.x += UnityEngine.Random.Range(-0.1f, 0.1f);
        spawnPos.z += UnityEngine.Random.Range(-0.1f, 0.1f);

        var res = Instantiate(GetNextEnemyPrefab(), spawnPos, transform.rotation);
        res.SetActive(true);

        res.transform.LookAt(PlayerAvatar.GetInstance.transform);
        var e = transform.eulerAngles;
        e.x = 0f;
        e.z = 0f;
        transform.eulerAngles = e;
        var entity = res.GetComponent<IEntity>();
        var deathEffect = res.GetComponentInChildren<DeathEffect>();
        deathEffect.portal = this;

        spawnedList.Add(entity);

        onEnemySpawnedEvent.Invoke (entity);

        res.transform.SetParent(transform);
    }

    public TemplateEvent<IEntity> onEnemySpawnedEvent = new TemplateEvent<IEntity>();
}
