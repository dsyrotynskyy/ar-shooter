﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UPlayerManaBar : MonoBehaviour
{
    public Image foreground;

    EntityMana _hp;
    PlayerAvatar _player;
    void Start()
    {
        _player = PlayerAvatar.GetInstance;
        _player.AttackEnabledChangedEvent.AddListener(UpdateUI);
        _hp = _player.GetComponent<EntityMana> ();
        _hp.OnManaChangedEvent.AddListener(UpdateUI);

        UpdateUI();
    }

    float _targetStat = 1f;

    void UpdateUI()
    {
        gameObject.SetActive(_player.InGame);
        _targetStat = _hp.ManaPercent;
    }

    void FixedUpdate()
    {
        foreground.fillAmount = Mathf.Lerp(foreground.fillAmount, _targetStat, Time.fixedDeltaTime * 10f);
    }
}
