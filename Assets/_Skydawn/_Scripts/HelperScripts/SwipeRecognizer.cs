﻿using UnityEngine;
using System.Collections;
using System;

public enum Direction
{
    Up = 0,
    Down = 1,
    Right = 2,
    Left = 3,
    None = 4
}

public class SwipeRecognizer : MonoBehaviour
{
    public enum SwipeState
    {
        Prepared, Performed, Released
    }

    SwipeState swipeState = SwipeState.Released;

    //    public event Action<SwipeDirection> Swipe;
    public event Action<Vector2, Direction> OnSwipeDotsContinue;
    public event Action<Vector2, Direction> OnSwipeInchesContinue;
    public event Action<Direction> OnSwipeEnded;

    protected Direction SwipeDirection { get; private set; }
    public bool swipeDirectionSelected { get; private set; }

	//calculate physical inches with pythagoras theorem
	public static float DeviceDiagonalSizeInInches()
	{
		float screenWidth = Screen.width / Screen.dpi;
		float screenHeight = Screen.height / Screen.dpi;
		float diagonalInches = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));

//		Debug.Log("Getting device inches: " + diagonalInches);

		return diagonalInches;
	}

    void LateUpdate()
    {
        if (swipeState == SwipeState.Released)
        {
            if (Input.GetMouseButtonDown(0) || Input.GetMouseButton (0))
            {
                fingerStart = Input.mousePosition;
                fingerEnd = fingerStart;
                swipeState = SwipeState.Prepared;
                swipeDirectionSelected = false;
            }
        } else if (swipeState == SwipeState.Prepared)
        {
            if ((Input.GetMouseButton(0) || Input.GetMouseButtonUp(0)))
            {              
				if (!swipeDirectionSelected)
				{
                    swipeDirectionSelected = SelectInitialSwipeDirection ();
                }

                ContinueSwipe();
            }
        }
        else if (swipeState == SwipeState.Performed)
        {
        }

        if (Input.GetMouseButtonDown(0) || Input.GetMouseButton(0) || Input.GetMouseButtonUp(0))
        {
        }
        else
        {
            if (swipeState != SwipeState.Released)
            {
                ContinueSwipe();

                if (OnSwipeEnded != null)
                {
					var direction = SwipeDirection;
					direction = GetSwipeDirection (GetMouseDelta);
					OnSwipeEnded(direction);
                }
                swipeState = SwipeState.Released;
				fingerStart = Vector2.zero;
				fingerEnd = Vector2.zero;
				swipeDirectionSelected = false;
            }
        }
    }

    bool SelectInitialSwipeDirection ()
    {
        Vector2 mouseDelta = GetMouseDelta;

        SwipeDirection = GetSwipeDirection(mouseDelta);
		return true;
    }

    void ContinueSwipe ()
    {
        Vector2 delta = Vector2.zero;
  
        if (swipeDirectionSelected)
        {
            Vector2 mouseDelta = GetMouseDelta;
            Vector2 normalizedDelta = NormalizeMouseDelta(mouseDelta);
            SwipeDirection = GetSwipeDirection(normalizedDelta);
            delta = mouseDelta;
        }
			
		if (OnSwipeDotsContinue != null) {
			OnSwipeDotsContinue(delta, SwipeDirection);
		}

		float sensetivityCoef = 3f;
        Vector2 swipeActual = delta;
        //bool moveCardAfterSwipe = false;
        //if (moveCardAfterSwipe) {

        //} else {

        //}

        //swipeActual = delta / Screen.dpi * sensetivityCoef;

        //if (Input.touchCount > 0)
        //{
        //    swipeActual *= 0.75f;
        //}

        //if (Application.isMobilePlatform)
        //{
        //    swipeActual *= 2.75f;
        //}

        //        swipeActual /= 10f;
        swipeActual = swipeActual / Screen.height * 24.5f;

		if (OnSwipeInchesContinue != null) {
			OnSwipeInchesContinue(swipeActual, SwipeDirection);
		}
    }

    Vector2 NormalizeMouseDelta (Vector2 actualDelta)
    {
        float x = 0f;
        float y = 0f;
        //switch (targetDirection)
        //{
        //    case (SwipeDirection.Left): x = Mathf.Min(actualDelta.x, 0f); break;
        //    case (SwipeDirection.Up): y = Mathf.Max(actualDelta.y, 0f); break;
        //    case (SwipeDirection.Right): x = Mathf.Max(actualDelta.x, 0f); break;
        //    case (SwipeDirection.Down): y = Mathf.Min(actualDelta.y, 0f); break;
        //}

        switch (SwipeDirection)
        {
            case (Direction.Left): x = actualDelta.x; break;
            case (Direction.Up): y = actualDelta.y; break;
            case (Direction.Right): x = actualDelta.x; break;
            case (Direction.Down): y = actualDelta.y; break;
        }

        //x = actualDelta.x;
        //y = actualDelta.y;

        Vector2 newMouseDelta = new Vector2(x, y);

        return newMouseDelta;
    }

    public Vector2 GetMouseDelta
    {
        get
        {
            fingerEnd = Input.mousePosition;
            Vector2 direction = fingerEnd - fingerStart;
            return direction;
        }
    }

    Direction GetSwipeDirection(Vector2 direction)
    {
        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
        {
            if (direction.x > 0)
                return (Direction.Right);
            else
                return (Direction.Left);
        }
        else {
            if (direction.y > 0)
                return (Direction.Up);
            else
                return (Direction.Down);
        }
    }

    Vector2 fingerStart;
    Vector2 fingerEnd;
}
