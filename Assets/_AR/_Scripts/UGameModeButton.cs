﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UGameModeButton : MonoBehaviour
{
    public Button placeEnemyButton;
    public GameObject attackButtonRoot;
    public Button gameModeButton;
    public GameObject placer;

    void Start()
    {
        gameModeButton.onClick.AddListener(ToggleGameMode);
        ToggleGameMode();
    }

    private void ToggleGameMode()
    {
        PlayerAvatar.GetInstance.InGame = !PlayerAvatar.GetInstance.InGame;
        placer.gameObject.SetActive(!PlayerAvatar.GetInstance.InGame);

        placeEnemyButton.gameObject.SetActive(!PlayerAvatar.GetInstance.InGame);
        attackButtonRoot.SetActive(PlayerAvatar.GetInstance.InGame);
    }
}
