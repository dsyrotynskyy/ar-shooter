﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunAnimator : MonoBehaviour
{
    public Animator animator;
    public AvatarMagicSpawner gun;

    void Start()
    {
        gun.onSpellSpawnedEvent.AddListener(OnSpellSpawned);
    }

    private void OnSpellSpawned()
    {
        animator.Play("Fire", -1, 0f);
    }
}
