﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PrefabSingleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private void Awake ()
    {
        if (_instance != null && _instance != this)
        {
            Debug.LogWarning("Duplicate singleton!!!");
            return;
        }
        _instance = (T)((System.Object)this);
        OnAwake();
    }

    protected virtual void OnAwake ()
    {

    }

    private static T _instance;

    public static bool HasInstance
    {
        get
        {
            return _instance != null;
        }
    }

    public static T GetInstance
    {
        get
        {
            if (_instance == null)
            {
                _instance = (T)FindObjectOfType(typeof(T));

//                Debug.LogError("Bad game architecture");

                //if (FindObjectsOfType(typeof(T)).Length > 1)
                //{
                //    Debug.LogError("[Singleton] Something went really wrong " +
                //        " - there should never be more than 1 singleton!" +
                //        " Reopening the scene might fix it.");
                //    return _instance;
                //}

                if (_instance == null)
                {
                    Debug.LogError("Not found instance of object!!!");
                } 
            }

            return _instance;
        }
    }
}
