﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeviceHelper  {

	static bool iPhoneXCalculated = false;
	static bool isIPhoneX = false;

	public static bool IsIPhoneX {
		get {
			bool res;
			if (!iPhoneXCalculated) {
				float iPhoneXRation = (float)2436 / 1125;
				float actualRation = (float)Screen.height / Screen.width;

				int i1 = (int)(iPhoneXRation * 100f);
				int i2 = (int)(actualRation * 100f);

				if (i1 <= i2) {
					isIPhoneX = true;
				}
				iPhoneXCalculated = true;
			}
            res = isIPhoneX;
			return res;
		}
	}
}
